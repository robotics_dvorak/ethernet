#include <string.h>
#include <sys/socket.h> // socket( domain, type, protocol );
#include <linux/if.h> // struct ifreq
#include <unistd.h>
#include <sys/ioctl.h> // ioctl, SIOCGIFHWADDR
#include <net/if_arp.h>

namespace net_tools {
  namespace ARP {
    struct packet {
      unsigned short h_type; // Hardware address type
      unsigned short p_type; // Protocol address type
      unsigned char  h_len;  // Hardware address length
      unsigned char  p_len;  // Protocol address length
      unsigned short operation; // request or response
      unsigned char src_hwaddr[6]; // Hardware address of sender
      unsigned char src_protoaddr[4]; // Protocol address of sender
      unsigned char dst_hwaddr[6]; // Hardware address of target
      unsigned char dst_protoaddr[4]; // Protocol address of target
    };
    union buffer {
      packet info;
      unsigned char data[28];
    };
  }

  namespace Ethernet {
    struct frame {
      unsigned char dst_hwaddr[6];
      unsigned char src_hwaddr[6];
      unsigned short ethertype;
      unsigned char payload[1500];
    };
    union buffer {
      frame info;
      unsigned char data[1514];
    };
  }

  unsigned char*  get_mac_addr( int socket_fd, const char if_name[IFNAMSIZ] ); 
  int  get_interface_index( int socket_fd, const char if_name[IFNAMSIZ] );
  int  get_socket_fd( int domain, int type, int protocol );
  int  send_eth_frame( Ethernet::buffer* eth_frame, int interface_index );
}
