SHELL = /usr/bin/zsh

OBJS  = build/main.o build/utils.o
CFLAG = -Wall -Wextra
CXX   = clang++
INCLUDE = ./include/

bin/main: ${OBJS}
	${CXX} ${CFLAG} -I${INCLUDE} ${OBJS} -o $@ 

build/main.o: src/main.cpp
	${CXX} ${CFLAG} -I${INCLUDE} -c $< -o $@

build/utils.o: src/utils.cpp
	${CXX} ${CFLAG} -I${INCLUDE} -c src/utils.cpp -o $@

clean:
	rm build/*.o

clear:
	rm bin/* build/*.o

run: bin/main
	sudo ./bin/main	
