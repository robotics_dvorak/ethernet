#include <iostream>
#include "utils.h"
#include <sys/types.h> // AF_PACKET
#include <linux/if_ether.h> // struct ethhdr
#include <linux/if_packet.h>

#include <arpa/inet.h> // htons, htonl, ntohs, ntohl

int main() {
  // Ideal process ------------
  net_tools::Ethernet::buffer eth_buffer {{{0},{0},0,{0}}};
  net_tools::ARP::buffer      arp_buffer {{0,0,0,0,0,{0},{0},{0},{0}}};
  const char interface_name[IFNAMSIZ] {"enp0s31f6"};

  int ethernet_socket = net_tools::get_socket_fd( AF_PACKET, SOCK_RAW, ETH_P_ALL );
  if ( ethernet_socket == -1 )
    return -1;

  // Fillout Ethernet Packet
  unsigned char* src_mac = get_mac_addr( ethernet_socket, interface_name );
  for (int i=0; i<6; i++) {
    eth_buffer.info.dst_hwaddr[i] = 0xff;
    eth_buffer.info.src_hwaddr = *src_mac[i];
  }
  eth_buffer.info.ethertype = htons( 0x0806 );

  // Fillout ARP Packet (pretend these are all in the right byte order)
  arp_buffer.h_type = 0x0001;
  arp_buffer.p_type = 0x0800;
  arp_buffer.h_len  = 0x06;
  arp_buffer.p_len  = 0x04;
  arp_buffer.operation = 0x01;
  // etc..

  // copy ARP packet int Ethernet payload

  int interface_index = net_tools::get_interface_index( ethernet_socket, interface_name );

  int bytes_sent = net_tools::send_eth_frame( &eth_buffer, interface_index );
  if ( bytes_sent == -1 )
    return -1;
  std::cout << bytes_sent << " bytes sent\n";
  // --------------------------
  const int size_payload = sizeof(ARP::packet);
  constexpr char interface_name[16] = "enp0s31f6";
  constexpr unsigned char src_protoaddr[4] = {192,168,86,68};
  constexpr unsigned char dst_protoaddr[4] = {192,168,86,1};
  Ethernet::buffer ethframe_buffer {{{0},{0},0,{0}}};
  ARP::buffer arp_packet {{ 0x0001, 0x0800, 0x0006, 0x0004, 0x0001, {0,0,0,0,0,0},{0,0,0,0},{0,0,0,0,0,0},{0,0,0,0}}};
  int ethernet_socket, raw_send;

  // Acquire Socket FD
  std::cout << "Attempting to acquire socket FD...\n";
  ethernet_socket = get_socket_fd( AF_PACKET, SOCK_RAW, ETH_P_ARP );
  if (ethernet_socket == -1)
    return -1;
  std::cout << "Acquired socket FD\n";

  // fill in sockaddr_ll
  sockaddr_ll socket_address {
    AF_PACKET,  // sll_family
    htons(ETH_P_ALL), // sll_protocol
    get_interface_index( ethernet_socket, interface_name ), // sll_ifindex
    1, 0, 6, {0}
  };

  // fill out the ARP request
  arp_packet.info.h_type = htons(0x0001);
  arp_packet.info.p_type = htons(0x0800);
  arp_packet.info.h_len  = 0x06;
  arp_packet.info.p_len  = 0x04;
  arp_packet.info.operation = htons(0x0001);
  
  unsigned char* src_mac = get_mac_addr( ethernet_socket, interface_name );//{0xe8,0x6a,0x64,0xa1,0x7c,0x3b};

  for (int i=0; i < 6; i++ ) {
    arp_packet.info.src_hwaddr[i] = src_mac[i];
    ethframe_buffer.info.src_hwaddr[i] = src_mac[i];
    ethframe_buffer.info.dst_hwaddr[i] = 0xff; // broadcast
  }
  for (int i=0; i<4; i++ ) {
    arp_packet.info.src_protoaddr[i] = src_protoaddr[i];
    arp_packet.info.dst_protoaddr[i] = dst_protoaddr[i];
  }

  // Fill out Ethernet header
  ethframe_buffer.info.ethertype = htons(0x0806);
  int i=0;
  for ( unsigned char a : arp_packet.data ) {
    ethframe_buffer.info.payload[i] = a;
    i++;
  }

  // Send the data
  raw_send = sendto( ethernet_socket, ethframe_buffer.data,
                     sizeof(ethframe_buffer), 0,
                     (struct sockaddr*) &socket_address,
                     sizeof(socket_address));

  if ( raw_send == -1 ) {
      std::cout << "sendto error: "<< strerror(errno) << '\n';
      return -1;
  }

  std::cout << raw_send << '\n';

  close( ethernet_socket );
  std::cout << "Closed socket\n";
}
