#include "utils.h"
#include <iostream>

namespace net_tools {
  int get_socket_fd( int domain, int type, int protocol ) {
    int socket_fd = socket( domain, type, protocol );
    if ( socket_fd == -1 ) {
      std::cerr << "ERROR: Cannot obtain socket: " << strerror(errno) << '\n';
      return -1;
    } else {
      return socket_fd;
    }
  }

  int get_interface_index( int socket_fd, const char if_name[IFNAMSIZ] ) {
    ifreq ifr {{0},{{0,{0}}}};

    for ( int i = 0; i < IFNAMSIZ-1; i++ ) // manual copy
      ifr.ifr_name[i] = if_name[i];

    ifr.ifr_name[IFNAMSIZ-1] = '\0';

    if (ioctl(socket_fd, SIOCGIFINDEX, &ifr) == -1) {
        std::cerr << "No such interface: "<< strerror(errno) << '\n';
        close(socket_fd);
        return -1;
    }

    ioctl(socket_fd, SIOCGIFFLAGS, &ifr);
    if ( (ifr.ifr_flags & 0x1) == 0) {
        std::cerr << "Interface is down: "<< strerror(errno) << '\n';
        close(socket_fd);
        return -1;
    }

    ioctl(socket_fd, SIOCGIFINDEX, &ifr);
    return ifr.ifr_ifindex;
  }

  unsigned char* get_mac_addr( int socket_fd, const char if_name[16] ) {
    ifreq ifr;
    unsigned char MAC_Address[6];
    unsigned char* MAC_Addr = &MAC_Address;

    for ( int i = 0; i < IFNAMSIZ-1; i++ ) // manual copy
      ifr.ifr_name[i] = if_name[i];
    ifr.ifr_name[IFNAMSIZ-1] = 0;

    if ( ioctl( socket_fd, SIOCGIFHWADDR, &ifr ) == -1 ) {
      std::cerr << "ERROR: " << strerror(errno) << '\n';
      return MAC_Addr;
    }
    if ( ifr.ifr_hwaddr.sa_family != 1 ) {
      std::cerr << "ERROR: Not an Ethernet interface\n";
      return MAC_Addr;
    }
    for (int i=0; i<6; i++)
      MAC_Address[i] = ifr.ifr_hwaddr.sa_data[i];
    return MAC_Addr;
  }

  int send_eth_frame( Ethernet::buffer* eth_frame, int interface_index ) {
    if (*eth_frame->info->
    raw_send = sendto( ethernet_socket, ethframe_buffer.data,
                    sizeof(ethframe_buffer), 0,
                    (struct sockaddr*) &socket_address,
                   sizeof(socket_address));
  }
}
